# Russian translation for ubuntu-filemanager-app
# Copyright (c) 2013 Rosetta Contributors and Canonical Ltd 2013
# This file is distributed under the same license as the ubuntu-filemanager-app package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2013.
#
msgid ""
msgstr ""
"Project-Id-Version: ubuntu-filemanager-app\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-07-03 13:35+0000\n"
"PO-Revision-Date: 2021-03-25 07:08+0000\n"
"Last-Translator: vokaliz <vokaliz@protonmail.com>\n"
"Language-Team: Russian <https://translate.ubports.com/projects/ubports/"
"filemanager-app/ru/>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<="
"4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Weblate 3.11.3\n"
"X-Launchpad-Export-Date: 2017-04-08 06:04+0000\n"

#: ../src/app/qml/actions/AddBookmark.qml:6
msgid "Add bookmark"
msgstr "Добавить закладку"

#: ../src/app/qml/actions/ArchiveExtract.qml:6
#: ../src/app/qml/dialogs/OpenArchiveDialog.qml:18
#: ../src/app/qml/dialogs/OpenWithDialog.qml:33
msgid "Extract archive"
msgstr "Распаковать архив"

#: ../src/app/qml/actions/Delete.qml:5
#: ../src/app/qml/dialogs/ConfirmMultipleDeleteDialog.qml:14
#: ../src/app/qml/dialogs/ConfirmSingleDeleteDialog.qml:15
#: ../src/app/qml/panels/SelectionBottomBar.qml:26
msgid "Delete"
msgstr "Удалить"

#: ../src/app/qml/actions/FileClearSelection.qml:8
msgid "Clear clipboard"
msgstr "Очистить буфер обмена"

#: ../src/app/qml/actions/FileCopy.qml:5
#: ../src/app/qml/panels/SelectionBottomBar.qml:45
msgid "Copy"
msgstr "Копировать"

#: ../src/app/qml/actions/FileCut.qml:5
#: ../src/app/qml/panels/SelectionBottomBar.qml:60
msgid "Cut"
msgstr "Вырезать"

#: ../src/app/qml/actions/FilePaste.qml:30
#, qt-format
msgid "Paste %1 file"
msgid_plural "Paste %1 files"
msgstr[0] "Вставить %1 файл"
msgstr[1] "Вставить %1 файла"
msgstr[2] "Вставить %1 файлов"

#: ../src/app/qml/actions/GoBack.qml:5
msgid "Go back"
msgstr "Назад"

#: ../src/app/qml/actions/GoNext.qml:5
msgid "Go next"
msgstr "Вперёд"

#: ../src/app/qml/actions/GoTo.qml:6
msgid "Go To"
msgstr "Перейти в"

#: ../src/app/qml/actions/NewItem.qml:6
msgid "New Item"
msgstr "Новый элемент"

#: ../src/app/qml/actions/PlacesBookmarks.qml:6
#: ../src/app/qml/ui/PlacesPage.qml:16
msgid "Places"
msgstr "Места"

#: ../src/app/qml/actions/Properties.qml:6
#: ../src/app/qml/dialogs/OpenWithDialog.qml:54
msgid "Properties"
msgstr "Свойства"

#: ../src/app/qml/actions/Rename.qml:5
#: ../src/app/qml/dialogs/ConfirmRenameDialog.qml:20
msgid "Rename"
msgstr "Переименовать"

#: ../src/app/qml/actions/Select.qml:5
#: ../src/app/qml/ui/FolderListPagePickModeHeader.qml:60
msgid "Select"
msgstr "Выбрать"

#: ../src/app/qml/actions/SelectUnselectAll.qml:6
msgid "Select None"
msgstr "Ничего не выбрано"

#: ../src/app/qml/actions/SelectUnselectAll.qml:6
msgid "Select All"
msgstr "Выбрать всё"

#: ../src/app/qml/actions/Settings.qml:6
msgid "Settings"
msgstr "Настройки"

#: ../src/app/qml/actions/Share.qml:5
msgid "Share"
msgstr "Поделиться"

#: ../src/app/qml/actions/TabsAdd.qml:5
msgid "Add tab"
msgstr "Добавить вкладку"

#: ../src/app/qml/actions/TabsCloseThis.qml:5
msgid "Close this tab"
msgstr "Закрыть вкладку"

#: ../src/app/qml/actions/TabsOpenInNewTab.qml:5
msgid "Open in a new tab"
msgstr "Открыть в новой вкладке"

#: ../src/app/qml/actions/UnlockFullAccess.qml:6
msgid "Unlock"
msgstr "Разблокировать"

#: ../src/app/qml/authentication/FingerprintDialog.qml:26
#: ../src/app/qml/authentication/PasswordDialog.qml:43
#: ../src/app/qml/dialogs/NetAuthenticationDialog.qml:28
msgid "Authentication required"
msgstr "Требуется аутентификация"

#: ../src/app/qml/authentication/FingerprintDialog.qml:27
msgid "Use your fingerprint to access restricted content"
msgstr "Используйте отпечаток пальца для доступа к защищённому содержимому"

#. TRANSLATORS: "Touch" here is a verb
#: ../src/app/qml/authentication/FingerprintDialog.qml:55
msgid "Touch sensor"
msgstr "Сенсорный датчик"

#: ../src/app/qml/authentication/FingerprintDialog.qml:60
msgid "Use password"
msgstr "Используйте пароль"

#: ../src/app/qml/authentication/FingerprintDialog.qml:69
#: ../src/app/qml/authentication/PasswordDialog.qml:80
#: ../src/app/qml/dialogs/CreateItemDialog.qml:60
#: ../src/app/qml/dialogs/ExtractingDialog.qml:35
#: ../src/app/qml/dialogs/FileActionDialog.qml:45
#: ../src/app/qml/dialogs/FileOperationProgressDialog.qml:44
#: ../src/app/qml/dialogs/NetAuthenticationDialog.qml:96
#: ../src/app/qml/dialogs/OpenArchiveDialog.qml:38
#: ../src/app/qml/dialogs/OpenWithDialog.qml:64
#: ../src/app/qml/dialogs/templates/ConfirmDialog.qml:41
#: ../src/app/qml/dialogs/templates/ConfirmDialogWithInput.qml:60
#: ../src/app/qml/ui/FileDetailsPopover.qml:184
#: ../src/app/qml/ui/FolderListPagePickModeHeader.qml:51
#: ../src/app/qml/ui/FolderListPageSelectionHeader.qml:26
msgid "Cancel"
msgstr "Отменить"

#: ../src/app/qml/authentication/FingerprintDialog.qml:126
msgid "Authentication failed!"
msgstr "Ошибка аутентификации!"

#: ../src/app/qml/authentication/FingerprintDialog.qml:137
msgid "Please retry"
msgstr "Повторите попытку"

#: ../src/app/qml/authentication/PasswordDialog.qml:35
msgid "Authentication failed"
msgstr "Ошибка аутентификации"

#: ../src/app/qml/authentication/PasswordDialog.qml:45
msgid "Your passphrase is required to access restricted content"
msgstr "Для доступа к защищённому содержимому введите пароль"

#: ../src/app/qml/authentication/PasswordDialog.qml:46
msgid "Your passcode is required to access restricted content"
msgstr "Для доступа к защищённому содержимому введите пароль"

#: ../src/app/qml/authentication/PasswordDialog.qml:57
msgid "passphrase (default is 0000 if unset)"
msgstr "пароль (по умолчанию - 0000, если не установлено)"

#: ../src/app/qml/authentication/PasswordDialog.qml:57
msgid "passcode (default is 0000 if unset)"
msgstr "пароль (по умолчанию - 0000, если не установлено)"

#: ../src/app/qml/authentication/PasswordDialog.qml:69
msgid "Authentication failed. Please retry"
msgstr "Ошибка аутентификации! Попробуйте ещё раз"

#: ../src/app/qml/authentication/PasswordDialog.qml:74
msgid "Authenticate"
msgstr "Проверка подлинности"

#: ../src/app/qml/backend/FolderListModel.qml:123
#, qt-format
msgid "%1 file"
msgstr "%1 файл"

#: ../src/app/qml/backend/FolderListModel.qml:124
#, qt-format
msgid "%1 files"
msgstr "%1 файлов"

#: ../src/app/qml/backend/FolderListModel.qml:135
msgid "Folder"
msgstr "Папка"

#: ../src/app/qml/components/TextualButtonStyle.qml:48
#: ../src/app/qml/components/TextualButtonStyle.qml:56
#: ../src/app/qml/components/TextualButtonStyle.qml:63
msgid "Pick"
msgstr "Выбрать"

#: ../src/app/qml/content-hub/FileOpener.qml:27
msgid "Open with"
msgstr "Открыть в"

#: ../src/app/qml/dialogs/ConfirmExtractDialog.qml:15
msgid "Extract Archive"
msgstr "Распаковать архив"

#: ../src/app/qml/dialogs/ConfirmExtractDialog.qml:16
#, qt-format
msgid "Are you sure you want to extract '%1' here?"
msgstr "Вы действительно хотите распаковать '%1' прямо здесь?"

#: ../src/app/qml/dialogs/ConfirmMultipleDeleteDialog.qml:15
#: ../src/app/qml/dialogs/ConfirmSingleDeleteDialog.qml:16
#, qt-format
msgid "Are you sure you want to permanently delete '%1'?"
msgstr "Безвозвратно удалить '%1'?"

#: ../src/app/qml/dialogs/ConfirmMultipleDeleteDialog.qml:15
msgid "these files"
msgstr "эти файлы"

#: ../src/app/qml/dialogs/ConfirmMultipleDeleteDialog.qml:18
#: ../src/app/qml/dialogs/ConfirmSingleDeleteDialog.qml:21
msgid "Deleting files"
msgstr "Удаление файлов"

#: ../src/app/qml/dialogs/ConfirmRenameDialog.qml:21
msgid "Enter a new name"
msgstr "Введите новое имя"

#: ../src/app/qml/dialogs/CreateItemDialog.qml:13
msgid "Create Item"
msgstr "Создать файл"

#: ../src/app/qml/dialogs/CreateItemDialog.qml:14
msgid "Enter name for new item"
msgstr "Введите имя нового файла"

#: ../src/app/qml/dialogs/CreateItemDialog.qml:18
msgid "Item name"
msgstr "Имя элемента"

#: ../src/app/qml/dialogs/CreateItemDialog.qml:24
msgid "Create file"
msgstr "Создать файл"

#: ../src/app/qml/dialogs/CreateItemDialog.qml:43
msgid "Create Folder"
msgstr "Создать папку"

#: ../src/app/qml/dialogs/ExtractingDialog.qml:23
#, qt-format
msgid "Extracting archive '%1'"
msgstr "Распаковывается архив '%1'"

#: ../src/app/qml/dialogs/ExtractingDialog.qml:44
#: ../src/app/qml/dialogs/NotifyDialog.qml:27
#: ../src/app/qml/dialogs/templates/ConfirmDialog.qml:31
#: ../src/app/qml/dialogs/templates/ConfirmDialogWithInput.qml:49
msgid "OK"
msgstr "Да"

#: ../src/app/qml/dialogs/ExtractingDialog.qml:59
msgid "Extracting failed"
msgstr "Ошибка распаковки архива"

#: ../src/app/qml/dialogs/ExtractingDialog.qml:60
#, qt-format
msgid "Extracting the archive '%1' failed."
msgstr "При распаковке архива '%1' произошла ошибка."

#: ../src/app/qml/dialogs/FileActionDialog.qml:30
msgid "Choose action"
msgstr "Выбрать действие"

#: ../src/app/qml/dialogs/FileActionDialog.qml:31
#, qt-format
msgid "For file: %1"
msgstr "Для файла: %1"

#: ../src/app/qml/dialogs/FileActionDialog.qml:35
msgid "Open"
msgstr "Открыть"

#: ../src/app/qml/dialogs/FileOperationProgressDialog.qml:27
msgid "Operation in progress"
msgstr "Действие выполняется"

#: ../src/app/qml/dialogs/FileOperationProgressDialog.qml:29
msgid "File operation"
msgstr "Действие с файлом"

#: ../src/app/qml/dialogs/NetAuthenticationDialog.qml:45
msgid "User"
msgstr "Пользователь"

#: ../src/app/qml/dialogs/NetAuthenticationDialog.qml:58
msgid "Password"
msgstr "Пароль"

#: ../src/app/qml/dialogs/NetAuthenticationDialog.qml:72
msgid "Save password"
msgstr "Сохранить пароль"

#: ../src/app/qml/dialogs/NetAuthenticationDialog.qml:108
msgid "Ok"
msgstr "ОК"

#: ../src/app/qml/dialogs/OpenArchiveDialog.qml:8
msgid "Archive file"
msgstr "Файл архива"

#: ../src/app/qml/dialogs/OpenArchiveDialog.qml:9
msgid "Do you want to extract the archive here?"
msgstr "Вы действительно хотите распаковать архив прямо здесь?"

#: ../src/app/qml/dialogs/OpenArchiveDialog.qml:28
#: ../src/app/qml/dialogs/OpenWithDialog.qml:44
msgid "Open with another app"
msgstr "Открыть в другом приложении"

#: ../src/app/qml/dialogs/OpenWithDialog.qml:8
msgid "Open file"
msgstr "Открыть файл"

#: ../src/app/qml/dialogs/OpenWithDialog.qml:9
msgid "What do you want to do with the clicked file?"
msgstr "Что вы хотите сделать с выбраным файлом?"

#: ../src/app/qml/dialogs/OpenWithDialog.qml:22
msgid "Preview"
msgstr "Просмотреть"

#: ../src/app/qml/filemanager.qml:170
#, qt-format
msgid "File %1"
msgstr "Файл %1"

#: ../src/app/qml/filemanager.qml:170
#, qt-format
msgid "%1 Files"
msgstr "%1 файла(-ов)"

#: ../src/app/qml/filemanager.qml:171
#, qt-format
msgid "Saved to: %1"
msgstr "Записано в: %1"

#: ../src/app/qml/panels/DefaultBottomBar.qml:24
msgid "Paste files"
msgstr "Вставить файлы"

#: ../src/app/qml/ui/FileDetailsPopover.qml:34
msgid "Readable"
msgstr "Доступен для чтения"

#: ../src/app/qml/ui/FileDetailsPopover.qml:37
msgid "Writable"
msgstr "Доступен для записи"

#: ../src/app/qml/ui/FileDetailsPopover.qml:40
msgid "Executable"
msgstr "Исполняемый"

#: ../src/app/qml/ui/FileDetailsPopover.qml:100
msgid "Where:"
msgstr "Куда:"

#: ../src/app/qml/ui/FileDetailsPopover.qml:127
msgid "Created:"
msgstr "Создано:"

#: ../src/app/qml/ui/FileDetailsPopover.qml:141
msgid "Modified:"
msgstr "Изменён:"

#: ../src/app/qml/ui/FileDetailsPopover.qml:155
msgid "Accessed:"
msgstr "Последний доступ:"

#: ../src/app/qml/ui/FileDetailsPopover.qml:176
msgid "Permissions:"
msgstr "Права доступа:"

#: ../src/app/qml/ui/FolderListPage.qml:241
#: ../src/app/qml/ui/FolderListPage.qml:296
msgid "Restricted access"
msgstr "Ограниченный доступ"

#: ../src/app/qml/ui/FolderListPage.qml:245
msgid ""
"Authentication is required in order to see all the content of this folder."
msgstr "Для просмотра всего содержимого этой папки требуется аутентификация."

#: ../src/app/qml/ui/FolderListPage.qml:282
msgid "No files"
msgstr "Файлы отсутствуют"

#: ../src/app/qml/ui/FolderListPage.qml:283
msgid "This folder is empty."
msgstr "Эта папка пуста."

#: ../src/app/qml/ui/FolderListPage.qml:297
msgid "Authentication is required in order to see the content of this folder."
msgstr "Для просмотра содержимого этой папки требуется аутентификация."

#: ../src/app/qml/ui/FolderListPage.qml:313
msgid "File operation error"
msgstr "Ошибка при обработке файла"

#: ../src/app/qml/ui/FolderListPageDefaultHeader.qml:21
#: ../src/app/qml/ui/FolderListPagePickModeHeader.qml:21
#, qt-format
msgid "%1 item"
msgid_plural "%1 items"
msgstr[0] "%1 элемент"
msgstr[1] "%1 элемента"
msgstr[2] "%1 элементов"

#: ../src/app/qml/ui/FolderListPagePickModeHeader.qml:21
msgid "Save here"
msgstr "Сохранить здесь"

#: ../src/app/qml/ui/FolderListPageSelectionHeader.qml:18
#, qt-format
msgid "%1 item selected"
msgid_plural "%1 items selected"
msgstr[0] "Выбран %1 элемент"
msgstr[1] "Выбрано %1 элемента"
msgstr[2] "Выбрано %1 элементов"

#: ../src/app/qml/ui/ViewPopover.qml:24
msgid "Show Hidden Files"
msgstr "Показать скрытые файлы"

#: ../src/app/qml/ui/ViewPopover.qml:37
msgid "View As"
msgstr "Отобразить как"

#: ../src/app/qml/ui/ViewPopover.qml:39
msgid "List"
msgstr "Список"

#: ../src/app/qml/ui/ViewPopover.qml:39
msgid "Icons"
msgstr "Значки"

#: ../src/app/qml/ui/ViewPopover.qml:44
msgid "Grid size"
msgstr "Размер сетки"

#: ../src/app/qml/ui/ViewPopover.qml:47 ../src/app/qml/ui/ViewPopover.qml:55
msgid "S"
msgstr "Ю"

#: ../src/app/qml/ui/ViewPopover.qml:47 ../src/app/qml/ui/ViewPopover.qml:55
msgid "M"
msgstr "Сред."

#: ../src/app/qml/ui/ViewPopover.qml:47 ../src/app/qml/ui/ViewPopover.qml:55
msgid "L"
msgstr "Больш."

#: ../src/app/qml/ui/ViewPopover.qml:47 ../src/app/qml/ui/ViewPopover.qml:55
msgid "XL"
msgstr "Оч. Больш."

#: ../src/app/qml/ui/ViewPopover.qml:52
msgid "List size"
msgstr "Размер списка"

#: ../src/app/qml/ui/ViewPopover.qml:60
msgid "Sort By"
msgstr "Сортировать по"

#: ../src/app/qml/ui/ViewPopover.qml:62
msgid "Name"
msgstr "Название"

#: ../src/app/qml/ui/ViewPopover.qml:62
msgid "Date"
msgstr "Дата"

#: ../src/app/qml/ui/ViewPopover.qml:62
msgid "Size"
msgstr "Размер"

#: ../src/app/qml/ui/ViewPopover.qml:67
msgid "Sort Order"
msgstr "Сортировка"

#: ../src/app/qml/ui/ViewPopover.qml:74
msgid "Theme"
msgstr "Тема"

#: ../src/app/qml/ui/ViewPopover.qml:76
msgid "Light"
msgstr "Светлая"

#: ../src/app/qml/ui/ViewPopover.qml:76
msgid "Dark"
msgstr "Тёмная"

#: ../src/app/qml/views/FolderDelegateActions.qml:40
msgid "Folder not accessible"
msgstr "Папка недоступна"

#. TRANSLATORS: this refers to a folder name
#: ../src/app/qml/views/FolderDelegateActions.qml:42
#, qt-format
msgid "Can not access %1"
msgstr "Нет доступа к %1"

#: ../src/app/qml/views/FolderDelegateActions.qml:136
msgid "Could not rename"
msgstr "Не удалось переименовать"

#: ../src/app/qml/views/FolderDelegateActions.qml:137
msgid ""
"Insufficient permissions, name contains special chars (e.g. '/'), or already "
"exists"
msgstr ""
"Недостаточно прав, имя содержит специальные символы (например, «/») или уже "
"существует"

#: ../src/app/qml/views/FolderListView.qml:72
msgid "Directories"
msgstr "Папки"

#: ../src/app/qml/views/FolderListView.qml:72
msgid "Files"
msgstr "Файлы"

#: ../src/plugin/folderlistmodel/dirmodel.cpp:360
msgid "Unknown"
msgstr "Неизвестный"

#: ../src/plugin/folderlistmodel/dirmodel.cpp:487
msgid "path or url may not exist or cannot be read"
msgstr "путь или url не существует, либо не может быть прочитан"

#: ../src/plugin/folderlistmodel/dirmodel.cpp:690
msgid "Rename error"
msgstr "Ошибка при переименовании"

#: ../src/plugin/folderlistmodel/dirmodel.cpp:713
msgid "Error creating new folder"
msgstr "Ошибка создания новой папки"

#: ../src/plugin/folderlistmodel/dirmodel.cpp:740
msgid "Touch file error"
msgstr "Ошибка при обработке"

#: ../src/plugin/folderlistmodel/dirmodel.cpp:1353
msgid "items"
msgstr "элементы"

#: ../src/plugin/folderlistmodel/filesystemaction.cpp:320
msgid "File or Directory does not exist"
msgstr "Файл или Папка не существует"

#: ../src/plugin/folderlistmodel/filesystemaction.cpp:321
msgid " does not exist"
msgstr " не существует"

#: ../src/plugin/folderlistmodel/filesystemaction.cpp:326
msgid "Cannot access File or Directory"
msgstr "Нет доступа к Файлу или Папке"

#: ../src/plugin/folderlistmodel/filesystemaction.cpp:327
msgid " it needs Authentication"
msgstr " необходима авторизация"

#: ../src/plugin/folderlistmodel/filesystemaction.cpp:361
msgid "Cannot copy/move items"
msgstr "Не удаётся скопировать/переместить элементы"

#: ../src/plugin/folderlistmodel/filesystemaction.cpp:362
#: ../src/plugin/folderlistmodel/filesystemaction.cpp:1487
msgid "no write permission on folder "
msgstr "нет прав на запись в папку "

#: ../src/plugin/folderlistmodel/filesystemaction.cpp:642
msgid "Could not remove the item "
msgstr "Не удалось удалить элемент "

#: ../src/plugin/folderlistmodel/filesystemaction.cpp:680
msgid "Could not find a suitable name to backup"
msgstr "Не удалось создать подходящее имя для резервной копии"

#: ../src/plugin/folderlistmodel/filesystemaction.cpp:733
msgid "Could not create the directory"
msgstr "Не удалось создать папку"

#: ../src/plugin/folderlistmodel/filesystemaction.cpp:738
msgid "Could not create link to"
msgstr "Не удалось создать ссылку на"

#: ../src/plugin/folderlistmodel/filesystemaction.cpp:746
msgid "Could not set permissions to dir"
msgstr "Не удалось изменить права доступа для папки"

#: ../src/plugin/folderlistmodel/filesystemaction.cpp:756
msgid "Could not open file"
msgstr "Не удалось открыть файл"

#: ../src/plugin/folderlistmodel/filesystemaction.cpp:772
msgid "There is no space to copy"
msgstr "Недостаточно места для создания копии"

#: ../src/plugin/folderlistmodel/filesystemaction.cpp:780
msgid "Could not create file"
msgstr "Не удалось создать файл"

#: ../src/plugin/folderlistmodel/filesystemaction.cpp:837
msgid "Could not remove the directory/file "
msgstr "Не удалось удалить папку/файл "

#: ../src/plugin/folderlistmodel/filesystemaction.cpp:850
msgid "Could not move the directory/file "
msgstr "Не удалось переместить папку/файл "

#: ../src/plugin/folderlistmodel/filesystemaction.cpp:1089
msgid "Write error in "
msgstr "Ошибка записи в "

#: ../src/plugin/folderlistmodel/filesystemaction.cpp:1101
msgid "Read error in "
msgstr "Ошибка чтения "

#: ../src/plugin/folderlistmodel/filesystemaction.cpp:1318
msgid " Copy"
msgstr " Копировать"

#: ../src/plugin/folderlistmodel/filesystemaction.cpp:1371
msgid "Set permissions error in "
msgstr "Ошибка изменения прав доступа к "

#: ../src/plugin/folderlistmodel/filesystemaction.cpp:1437
msgid "Could not create trash info file"
msgstr "Не удалось создать файл с информацией о корзине"

#: ../src/plugin/folderlistmodel/filesystemaction.cpp:1449
msgid "Could not remove the trash info file"
msgstr "Не удалось удалить файл с информацией о корзине"

#: ../src/plugin/folderlistmodel/filesystemaction.cpp:1479
#: ../src/plugin/folderlistmodel/filesystemaction.cpp:1486
msgid "Cannot move items"
msgstr "Не удалось переместить элементы"

#: ../src/plugin/folderlistmodel/filesystemaction.cpp:1481
msgid "origin and destination folders are the same"
msgstr "папка назначения совпадает с папкой источника"

#: ../src/plugin/folderlistmodel/filesystemaction.cpp:1524
msgid "There is no space to download"
msgstr "Недостаточно места для скачивания"

#: ../src/plugin/folderlistmodel/smb/qsambaclient/src/smbusershare.cpp:72
msgid "net tool not found, check samba installation"
msgstr "утилита net не найдена, проверьте установлена ли samba"

#: ../src/plugin/folderlistmodel/smb/qsambaclient/src/smbusershare.cpp:80
msgid "cannot write in "
msgstr "не удаётся записать в "

#: com.ubuntu.filemanager.desktop.in.in.h:1
msgid "File Manager"
msgstr "Менеджер файлов"

#: com.ubuntu.filemanager.desktop.in.in.h:2
msgid "folder;manager;explore;disk;filesystem;"
msgstr "папка;управление;открыть;диск;файловая система;"

#~ msgid "About Application"
#~ msgstr "О приложении"

#~ msgid ""
#~ "The <b>Application</b> example demonstrates how to write modern GUI "
#~ "applications using Qt, with a menu bar, toolbars, and a status bar."
#~ msgstr ""
#~ "Пример <b>Приложение</b> демонстрирует, как написать современное "
#~ "графическое приложение с использованием Qt. В этом примере используются "
#~ "меню, панели инструментов и строка статуса."

#~ msgid "&New"
#~ msgstr "&Создать"

#~ msgid "Create a new file"
#~ msgstr "Создать новый файл"

#~ msgid "&Open..."
#~ msgstr "&Открыть..."

#~ msgid "Open an existing file"
#~ msgstr "Открыть существующий файл"

#~ msgid "&Save"
#~ msgstr "Со&хранить"

#~ msgid "Save the document to disk"
#~ msgstr "Сохранить документ"

#~ msgid "Save &As..."
#~ msgstr "Сохранить &как..."

#~ msgid "Save the document under a new name"
#~ msgstr "Сохранить документ под другим названием"

#~ msgid "E&xit"
#~ msgstr "В&ыход"

#~ msgid "Exit the application"
#~ msgstr "Выйти из приложения"

#~ msgid "Cu&t"
#~ msgstr "Вырезать"

#~ msgid "Cut the current selection's contents to the clipboard"
#~ msgstr "Вырезать выделенный контент и поместить его в буфер обмена"

#~ msgid "&Copy"
#~ msgstr "Копировать"

#~ msgid "Copy the current selection's contents to the clipboard"
#~ msgstr "Скопировать выделенный контент в буфер обмена"

#~ msgid "&Paste"
#~ msgstr "Вставить"

#~ msgid "Paste the clipboard's contents into the current selection"
#~ msgstr "Заменить выделенный контент содержимым буфера обмена"

#~ msgid "&About"
#~ msgstr "&О приложении"

#~ msgid "Show the application's About box"
#~ msgstr "Показать всплывающее окно с информацией о приложении"

#~ msgid "About &Qt"
#~ msgstr "Информация о &Qt"

#~ msgid "Show the Qt library's About box"
#~ msgstr "Показать всплывающее окно с информацией о библиотеках Qt"

#~ msgid "&File"
#~ msgstr "&Файл"

#~ msgid "&Edit"
#~ msgstr "&Правка"

#~ msgid "&Help"
#~ msgstr "&Справка"

#~ msgid "Edit"
#~ msgstr "Правка"

#~ msgid "Ready"
#~ msgstr "Готово"

#~ msgid "Application"
#~ msgstr "Приложение"

#~ msgid ""
#~ "The document has been modified.\n"
#~ "Do you want to save your changes?"
#~ msgstr ""
#~ "Документ изменён.\n"
#~ "Сохранить изменения?"

#~ msgid ""
#~ "Cannot read file %1:\n"
#~ "%2."
#~ msgstr ""
#~ "Ошибка при загрузке файла %1:\n"
#~ "%2."

#~ msgid "File loaded"
#~ msgstr "Файл загружен"

#~ msgid ""
#~ "Cannot write file %1:\n"
#~ "%2."
#~ msgstr ""
#~ "Ошибка записи файла %1:\n"
#~ "%2."

#~ msgid "File saved"
#~ msgstr "Файл записан"

#~ msgid "%1 (%2 file)"
#~ msgid_plural "%1 (%2 files)"
#~ msgstr[0] "%1 (%2 файл)"
#~ msgstr[1] "%1 (%2 файла)"
#~ msgstr[2] "%1 (%2 файлов)"

#~ msgid "Device"
#~ msgstr "Устройство"

#~ msgid "Change app settings"
#~ msgstr "Настройки приложения"

#~ msgid "password"
#~ msgstr "пароль"

#~ msgid "Path:"
#~ msgstr "Путь:"

#~ msgid "Contents:"
#~ msgstr "Содержимое:"

#~ msgid "Unlock full access"
#~ msgstr "Открыть доступ без ограничений"

#~ msgid "Enter name for new folder"
#~ msgstr "Введите имя новой папки"

#~ msgid "~/Desktop"
#~ msgstr "~/Рабочий стол"

#~ msgid "~/Public"
#~ msgstr "~/Public"

#~ msgid "~/Programs"
#~ msgstr "~/Программы"

#~ msgid "~/Templates"
#~ msgstr "~/Шаблоны"

#~ msgid "Home"
#~ msgstr "Домашний"

#~ msgid "Network"
#~ msgstr "Сеть"

#~ msgid "Go To Location"
#~ msgstr "Перейти в"

#~ msgid "Enter a location to go to:"
#~ msgstr "Перейти по адресу:"

#~ msgid "Location..."
#~ msgstr "Адрес..."

#~ msgid "Go"
#~ msgstr "Перейти"

#~ msgid "Show Advanced Features"
#~ msgstr "Показать расширенные возможности"

#~ msgid "Ascending"
#~ msgstr "по возрастанию"

#~ msgid "Descending"
#~ msgstr "по убыванию"

#~ msgid "Filter"
#~ msgstr "Фильтр"
